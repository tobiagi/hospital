/**
 * Class used to define a nurse
 */
public class Nurse extends Employee {
    public Nurse(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }
    @Override
    public String toString(){
        return getFullName();
    }
}
