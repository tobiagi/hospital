/**
 * Interface to set diagnosis to patient
 */
public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
