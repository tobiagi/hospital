/**
 * A simple client to test some methods
 */
public class HospitalClient {
    public static void main(String[] args) {

        /**
         * Creates a new hospital and fills it with information
         */
        Hospital hospital = new Hospital("Attaway General");
        HospitalTestData.fillRegisterWithTestData(hospital);

        /**
         * Creates an employee and adds him to ArrayList over employees
         */
        Employee ola = new Employee("Ola", "Nordmann", "1");
        hospital.getDepartments().get(0).addEmployee(ola);

        /**
         * Prints out all departments and employees in the first department
         */
        System.out.println(hospital.getDepartments());
        System.out.println(hospital.getDepartments().get(0).getEmployees());

        /**
         * test to remove an employee that exists from a department
         */
        try {
            hospital.getDepartments().get(0).remove(ola);
        } catch(RemoveException r) {
            System.err.println(r);
        }

        /**
         * Test to remove a patient that does not exist from a department
         */
        try {
            hospital.getDepartments().get(0).remove(new Patient("Odd Even", "Primtallet", ""));
        } catch(RemoveException r){
            System.err.println(r);
        }
    }
}
