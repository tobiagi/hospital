import java.util.ArrayList;

/**
 * Class to define hospitals and control ArrayLists over departments
 */
public class Hospital {
    private final String hospitalName;
    private final ArrayList<Department> department = new ArrayList<>();

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Method to return an ArrayList over departments (if there are any)
     * @return ArrayList of departments, or null
     */
    public ArrayList<Department> getDepartments() {
        if (department.size() > 0) return department;
        else return null;
    }

    /**
     * Method to add a department to an ArrayList of departments
     * @param p department to be added
     */
    public void addDepartment(Department p) {
        if (!department.contains(p)) {
            department.add(p);
        }
    }

    @Override
    public String toString() {
        return "Hospital name = "+ hospitalName;
    }
}
