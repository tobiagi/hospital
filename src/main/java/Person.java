/**
 * Class that defines a person
 * Contains get and set methods for everything that defines a person in this program
 */
public abstract class Person {

    private String firstname;
    private String lastName;
    private String socialSecurityNumber;

    public Person(String firstname, String lastName, String socialSecurityNumber){
        this.firstname = firstname;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return firstname + " " + lastName;
    }

    @Override
    public String toString() {
        return "Person{ " +
                "First name = '" + firstname + '\'' +
                ", Last name = '" + lastName + '\'' +
                ", Social security number = '" + socialSecurityNumber + '\'' +
                '}';
    }
}
