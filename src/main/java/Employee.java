/**
 * Class used to define an employee
 */
public class Employee extends Person {
    public Employee(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }

    @Override
    public String toString(){
        return getFullName();
    }
}
