/**
 * Class to define a patient
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis;

    public Patient(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }

    /**
     * Method to return a patients diagnosis
     * @return The patients diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Method to add a diagnosis to a patient
     * @param diagnosis to be set
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString(){
        return getFullName();
    }
}
