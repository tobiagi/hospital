/**
 * Class used to define a Surgeon
 */
public class Surgeon extends Doctor {

    public Surgeon(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }

    /**
     * Method to set a diagnosis to a patient
     * @param patient to set diagnosis to
     * @param diagnosis to be set
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
