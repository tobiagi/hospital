/**
 * Class used to define a general practitioner
 */
public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstname, String lastName, String socialSecurityNumber) {
        super(firstname, lastName, socialSecurityNumber);
    }

    /**
     * Method to set a diagnosis to a patient
     * @param patient to set diagnosis to
     * @param diagnosis to be set
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
