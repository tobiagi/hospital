import java.util.ArrayList;
import java.util.Objects;

/**
 * Class handles ArrayLists over patients, employees and defines departments
 */
public class Department {
    private String departmentName;
    private final ArrayList<Employee> employee = new ArrayList<>();
    private final ArrayList<Patient> patient = new ArrayList<>();

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Method to add employee to ArrayList over employees
     * @param e employee to be added
     */
    public void addEmployee(Employee e) {
        if (!employee.contains(e)) {
            employee.add(e);
        }
    }

    /**
     * Method to return all employees
     * @return ArrayList of employees
     */
    public ArrayList<Employee> getEmployees() {
        if (employee.size() > 0) return employee;
        else return null;
    }

    /**
     * Method to add patients to ArrayList over patients
     * @param p patient to be added
     */
    public void addPatient(Patient p) {
        if (!patient.contains(p)) {
            patient.add(p);
        }
    }

    /**
     * Method to return all patients
     * @return ArrayList of patients
     */
    public ArrayList<Patient> getPatients() {
        if (patient.size() > 0) return patient;
        else return null;
    }

    /**
     * Method to remove patients or employees from the ArrayList
     * @param p Person to be removed
     * @throws RemoveException if person does not exist in either ArrayList
     */
    public void remove(Person p) throws RemoveException {
        if (patient.contains(p)) {
            patient.remove(p);
        }
        else if (employee.contains(p)) {
            employee.remove(p);
        }
        else {
            throw new RemoveException("This person does not exist");
        }
    }

    /**
     * Method to compare the name of two departments
     * @param o the departments to be compared
     * @return True if name is the same, false if they are different
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        return "Department name = " + departmentName;
    }
}
