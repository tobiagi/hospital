/**
 * Class used to handle exceptions sent from the remove() method in Department.java
 */
public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String s) {
        super("This person does not exist");
    }
}
