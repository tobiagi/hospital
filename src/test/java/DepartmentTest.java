import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.util.function.DoublePredicate;

/**
 * Test class for several methods
 */
class DepartmentTest {

    /**
     * Tests the remove() method on an employee that exists in set department
     */
    @Test
    void remove() {
        Department department = new Department("testDepartment");
        Employee ola = new Employee("Ola", "Nordmann", "1");
        department.addEmployee(ola);
        try {
            department.remove(ola);
        } catch(RemoveException r) {
            System.err.println(r);
        }
    }

    /**
     * Tests the remove() method on an employee that does not exist in set department
     */
    @Test
    void remove1() {
        Department department = new Department("testDepartment");
        Employee ola = new Employee("Ola", "Nordmann", "1");
        try {
            department.remove(ola);
        } catch(RemoveException r) {
            System.err.println(r);
        }
    }

}